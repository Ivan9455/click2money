### 1 Собираем приложение
```make build ```

### 2 Заходим в контейнер
```make phpc```

### 3 Устанавливаем компосер
```composer install```

### 4 Инициализируем проект
```php init --env=Development --overwrite=All --delete=All```

### 5 Ставим миграции
```yes | php yii migrate```

### 6 Ставим фикстуры
```
yes | php yii fixture User
yes | php yii fixture Product
```

### Готово!