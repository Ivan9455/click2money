<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->updateCharsetDb('utf8_general_ci');
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }

    public function updateCharsetDb(string $charset)
    {
        $db = $this->db->createCommand("select database()")->queryOne()['database()'];
        Yii::$app->db->createCommand('ALTER DATABASE ' . $db . ' COLLATE ' . $charset)->execute();

        $tables = Yii::$app->db->schema->tableNames;

        $tables = array_flip($tables);
        unset($tables['auth_assignment'], $tables['auth_item'], $tables['auth_item_child'], $tables['auth_rule']);
        $tables = array_flip($tables);

        foreach ($tables as $table) {
            $this->db->createCommand("ALTER TABLE  {$db}.{$table} CONVERT TO CHARACTER SET utf8 COLLATE {$charset}")->execute();
        }
    }
}
