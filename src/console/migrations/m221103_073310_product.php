<?php

use yii\db\Migration;

/**
 * Class m221103_073310_product
 */
class m221103_073310_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey()->notNull()->unique(),
            'logo' => $this->string(2048)->notNull()->comment('Логотип продукта'),
            'sum_max' => $this->integer(11)->notNull()->comment('Максимальная сумма кредита'),
            'percent_min' => $this->decimal(3,2)->notNull()->comment('Процентная ставка в день'),
            'loan_term_min' => $this->integer(3)->notNull()->comment('Минимальный срок займа'),
            'loan_term_max' => $this->integer(3)->notNull()->comment('Максимальный срок займа'),
            'url' => $this->string(2048)->notNull()->comment('Ссылка на продукт'),
            'status' => $this->tinyInteger()->notNull()->comment('Статус'),
            'priority' => $this->tinyInteger()->notNull()->comment('Приоритет'),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m221103_073310_product cannot be reverted.\n";

        return false;
    }
}
