<?php

use common\models\Product;

/**
 * @var yii\web\View $this
 * @var array $products
 * @var Product $product
 */

$this->title = 'click2money';
?>
<div class="row">
    <?php foreach ($products as $product): ?>
        <div class="col text-center">
            <a href="<?= $product->url ?>" target="_blank" class="text-decoration-none text-reset">
                <img src="<?= $product->getLogo() ?>"/>
                <p>Сумма займа</p>
                <p>До <?= $product->sum_max?> руб.</p>
                <p>Процентная ставка</p>
                <p>От <?= $product->getPercentMin()?>% в день</p>
                <p>Срок займа</p>
                <p>От <?= $product->loan_term_min ?> до <?= $product->loan_term_max ?> дней</p>
                <div class="btn btn-success">Получить деньги</div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
