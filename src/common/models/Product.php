<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $logo Логотип продукта
 * @property int $sum_max Максимальная сумма кредита
 * @property float $percent_min Процентная ставка в день
 * @property int $loan_term_min Минимальный срок займа
 * @property int $loan_term_max Максимальный срок займа
 * @property string $url Ссылка на продукт
 * @property int $status Статус
 * @property int $priority Приоритет
 * @property string $created_at
 * @property string $updated_at
 */
class Product extends \yii\db\ActiveRecord
{
    public const STATUS_ENABLED = 1;
    public const STATUS_DISABLED = 2;

    public static function tableName(): string
    {
        return 'product';
    }

    public function behaviors() : array
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['logo', 'sum_max', 'percent_min', 'loan_term_min', 'loan_term_max', 'url', 'status', 'priority'], 'required'],
            [['sum_max', 'loan_term_min', 'loan_term_max', 'status', 'priority'], 'integer'],
            [['percent_min'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['logo', 'url'], 'string', 'max' => 2048],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'logo' => 'Лого',
            'sum_max' => 'Максимальная сумма',
            'percent_min' => 'Минимальный процент',
            'loan_term_min' => 'Минимальный срок',
            'loan_term_max' => 'Максимальный срок',
            'url' => ' url',
            'status' => 'Статус',
            'priority' => 'Приоритет',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function getLogo(): string
    {
        return getenv('ADMIN_URL') . $this->logo;
    }

    public function getPercentMin(): float
    {
        return $this->percent_min == 0 ? 0 : $this->percent_min;
    }

    public function getStatusText(): string
    {
        return self::getStatuses()[$this->status] ?? 'Невыбран';
    }

    public static function getStatuses() : array
    {
        return [
            self::STATUS_ENABLED => 'Активно',
            self::STATUS_DISABLED => 'Не активно'
        ];
    }
}
