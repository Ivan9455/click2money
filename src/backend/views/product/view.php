<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Product;

/** @var yii\web\View $this */
/** @var Product $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'logo',
                'format' => 'html',
                'value' => function (Product $product) {
                    return Html::img($product->getLogo());
                },
            ],
            'sum_max',
            'percent_min',
            'loan_term_min',
            'loan_term_max',
            'url:url',
            [
                'attribute' => 'status',
                'value' => function (Product $product) {
                    return $product->getStatusText();
                },
            ],
            'priority',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
