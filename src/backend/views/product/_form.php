<?php

use common\models\Product;
use yii\helpers\Html;
use yii\bootstrap5\ActiveForm;
use backend\models\ProductForm;

/** @var yii\web\View $this */
/** @var ProductForm $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="product-form">
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'logo')->fileInput() ?>

            <?= $form->field($model, 'sum_max')->textInput() ?>

            <?= $form->field($model, 'percent_min')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'loan_term_min')->textInput() ?>

            <?= $form->field($model, 'loan_term_max')->textInput() ?>

            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropdownList(Product::getStatuses()) ?>

            <?= $form->field($model, 'priority')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
