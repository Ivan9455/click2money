<?php

use common\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'logo',
                'format' => 'html',
                'value' => function (Product $product) {
                    return Html::img($product->getLogo());
                },
            ],
            'sum_max',
            [
                'attribute' => 'percent_min',
                'value' => function (Product $product) {
                    return $product->getPercentMin();
                },
            ],
            'loan_term_min',
            'loan_term_max',
            'url:url',
            [
                'attribute' => 'status',
                'value' => function (Product $product) {
                    return $product->getStatusText();
                },
            ],
            'priority',
            'created_at',
            'updated_at',
            [
                'class' => ActionColumn::class,
                'urlCreator' => function ($action, Product $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
