<?php

namespace backend\models;

use common\models\Product;
use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class ProductForm extends Model
{
    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';
    public $id;
    public $sum_max;
    public $loan_term_min;
    public $loan_term_max;
    public $status;
    public $priority;
    public $percent_min;
    public $url;
    protected $pathLogo;
    /**
     * @var Product
     */
    protected $product;
    /**
     * @var UploadedFile
     */
    public $logo;

    public function rules(): array
    {
        return [
            [['sum_max', 'percent_min', 'loan_term_min', 'loan_term_max', 'url', 'status', 'priority'], 'required'],
            [['sum_max', 'loan_term_min', 'loan_term_max', 'status', 'priority'], 'integer'],
            ['loan_term_min', 'integer', 'integerOnly' => true, 'min' => 1],
            ['loan_term_max', 'integer', 'integerOnly' => true, 'max' => 30],
            [['percent_min'], 'number'],
            [['url'], 'string', 'max' => 2048],
            [['logo'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'on' => self::SCENARIO_CREATE],
            [['logo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'on' => self::SCENARIO_UPDATE],
            [['loan_term_min'], 'validateLoanTerm'],
            [['logo'], 'required', 'on' => self::SCENARIO_CREATE]
        ];
    }

    public function validateLoanTerm($attribute, $params)
    {
        if ((int)$this->loan_term_max < (int)$this->loan_term_min) {
            $this->addError($attribute, 'Минимальное значение не должно быть больше максимального');
        }
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'logo' => 'Лого',
            'sum_max' => 'Максимальная сумма',
            'percent_min' => 'Минимальный процент',
            'loan_term_min' => 'Минимальный срок',
            'loan_term_max' => 'Максимальный срок',
            'url' => ' url',
            'status' => 'Статус',
            'priority' => 'Приоритет',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    public function scenarios(): array
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['logo', 'sum_max', 'percent_min', 'loan_term_min', 'loan_term_max', 'url', 'status', 'priority'];
        $scenarios[self::SCENARIO_UPDATE] = ['logo', 'sum_max', 'percent_min', 'loan_term_min', 'loan_term_max', 'url', 'status', 'priority'];
        return $scenarios;
    }

    public function upload(): bool
    {
        if ($this->logo = UploadedFile::getInstance($this, 'logo')) {
            $this->pathLogo = sprintf('images/product/%s.%s',
                Yii::$app->security->generateRandomString(),
                $this->logo->extension
            );
            return $this->logo->saveAs(sprintf('%s/web/%s', Yii::getAlias('@backend'), $this->pathLogo));
        }
        return false;
    }

    public function save(): bool
    {
        if (!$this->upload()) {
            $this->addError('logo', 'Не удалось загрузить файл');
            return false;
        }
        $product = new Product();
        $product->load(['Product' => $this->attributes]);
        $product->logo = $this->pathLogo;

        if (!$product->save()) {
            $this->addErrors($product->getErrors());
            return false;
        }
        $this->id = $product->id;
        return true;
    }

    public function update(): bool
    {
        $logo = $this->product->logo;
        $this->product->load(['Product' => $this->attributes]);
        if(UploadedFile::getInstance($this, 'logo') == null){
            $this->product->logo = $logo;
        } elseif ($this->upload()) {
            $this->product->logo = $this->pathLogo;
            unlink(sprintf('%s/web/%s', Yii::getAlias('@backend'),$logo));
        }else{
            $this->addError('logo', 'Не удалось загрузить файл');
            return false;
        }

        if (!$this->product->save()) {
            $this->addErrors($this->product->getErrors());
            return false;
        }
        return true;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
    }
}
